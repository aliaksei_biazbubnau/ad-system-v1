package org.al.adsystem.util;

public class Constant {
    public final static String LOGIN = "login";
    public static final String ERROR_MESSAGE_ATTR_NAME = "errorMessage";
    public static final String INCORRECT_DATA_ENTERED_MESSAGE =
            "Please, check data your entered. And try again.";
    public static final String INCORRECT_LOGIN_OR_PASSWORD_MESSAGE =
            "Please, enter valid login and password. And try again.";
    public static final String SUCCESSFUL_REGISTRATION_MESSAGE =
            "Registration successful! Please, sign in with new login and password.";
    public static final String INCORRECT_SEARCH_QUERY_MESSAGE =
            "Incorrect search query. Please enter another one and try again.";
    public static final String NOT_FOUND_ADS_MESSAGE = "Found nothing by your request";
    public static final String DEFAULT_USERNAME = "guest";
    public static final String INFORMATION_MESSAGE_ATTR_NAME = "infoMessage";
    public static final String USERNAME_ATTR_NAME = "username";
    public static final String USER_ID_ATTR_NAME = "userid";
    public static final Object VALUE_NULL = null;
    public static final int VALUE_0 = 0;
    public static final String EXIT_VALUE = "exit";
    public static final String MY_ADS_VALUE = "show-my-ads";
    public static final String CURRENT_ADS_LIST_NAME = "adsList";
    public static final String HEADER_FIELD = "header";
    public static final String BODY_FIELD = "body";
    public static final String CONTACT_FIELD = "contact";
}
