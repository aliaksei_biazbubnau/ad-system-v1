package org.al.adsystem.controller;

import org.al.adsystem.model.domain.bean.Advert;
import org.al.adsystem.service.iface.AdService;
import org.al.adsystem.util.UserDataValidator;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import javax.servlet.http.HttpSession;

import static org.al.adsystem.util.Constant.*;

@Controller
public class AdController {
    @Autowired
    private AdService service;
    private static final String INCORRECT_AD_DATA_ENTERED =
            "Please, fill all required fields.";

    @RequestMapping("/new-ad")
    public String newAdPage() {
        return "new-ad";
    }

    @RequestMapping(value = "/add-new-ad", method = RequestMethod.POST)
    public String addNewAd(final Model model, @RequestParam("ad-header") final String header,
                           @RequestParam("ad-body") final String body,
                           @RequestParam("ad-contact") final String contact, final HttpSession session) {
        if (UserDataValidator.validateAdData(header, body, contact)) {
            int targetId = (int) session.getAttribute(USER_ID_ATTR_NAME);
            Advert advert = new Advert(header, body, contact, targetId);
            service.addNewAdvert(advert);
        } else {
            model.addAttribute(ERROR_MESSAGE_ATTR_NAME, INCORRECT_AD_DATA_ENTERED);
            return "new-ad";
        }

        return "redirect:index";
    }

    public void setService(final AdService service) {
        this.service = service;
    }
}
