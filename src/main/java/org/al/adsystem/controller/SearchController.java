package org.al.adsystem.controller;

import org.al.adsystem.model.domain.bean.Advert;
import org.al.adsystem.service.iface.AdService;
import org.al.adsystem.util.QueryValidator;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import java.util.List;

import static org.al.adsystem.util.Constant.*;

@Controller
public class SearchController {
    @Autowired
    private AdService service;

    @RequestMapping("/search")
    public String doSearch(@RequestParam final String query, final Model model) {
        if (!QueryValidator.isQueryValid(query)) {
            model.addAttribute(ERROR_MESSAGE_ATTR_NAME, INCORRECT_SEARCH_QUERY_MESSAGE);
            return "forward:index";
        }
        List<Advert> adverts = service.getSearchResults(query);
        if (adverts.size()== VALUE_0) {
            model.addAttribute(INFORMATION_MESSAGE_ATTR_NAME, NOT_FOUND_ADS_MESSAGE);
            return "index";
        }
        adverts.forEach(advert -> System.out.println("2Page:" + advert.toString()));

        model.addAttribute(CURRENT_ADS_LIST_NAME, adverts);
        return "forward:search-result";
    }

    public void setService(AdService service) {
        this.service = service;
    }
}
