package org.al.adsystem.controller;

import org.al.adsystem.service.iface.UserService;
import org.al.adsystem.util.UserDataValidator;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import javax.servlet.http.HttpSession;

import static org.al.adsystem.util.Constant.*;

@Controller
public class LoginController {

    @Autowired
    private UserService service;

    @RequestMapping(value = "/login", method = RequestMethod.POST)
    public String doLogin(@RequestParam final String login, @RequestParam final String password,
                          final Model model, final HttpSession session) {
        if (!UserDataValidator.validateSignInData(login, password)) {
            model.addAttribute(ERROR_MESSAGE_ATTR_NAME, INCORRECT_LOGIN_OR_PASSWORD_MESSAGE);
            return "forward:index";
        }
        if (service.isLoginAllowed(login, password)) {
            session.setAttribute(USERNAME_ATTR_NAME, login);

            int userId = service.getUserIdByLogin(login);
            session.setAttribute(USER_ID_ATTR_NAME, userId);
        }
        return "redirect:index";
    }

    @RequestMapping("/user-action")
    public String doLogout(@RequestParam final String act, HttpSession session) {
        if (act != VALUE_NULL) {
            if (act.equals(EXIT_VALUE)) {
                session.invalidate();
            } else if (act.equals(MY_ADS_VALUE)){
                return "my-ads";
            }
        }
        return "redirect:index";
    }

    public void setService(final UserService service) {
        this.service = service;
    }
}
