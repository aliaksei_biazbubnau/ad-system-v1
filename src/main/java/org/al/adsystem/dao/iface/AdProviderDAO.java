package org.al.adsystem.dao.iface;

import org.al.adsystem.model.domain.bean.Advert;
import java.util.List;

public interface AdProviderDAO {

    List<Advert> getCurrentAds(int count);

    List<Advert> getUserAds(int id, int count);

    void addNewAdvert(Advert newAd);
}
