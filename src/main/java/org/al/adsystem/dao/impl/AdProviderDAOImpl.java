package org.al.adsystem.dao.impl;

import org.al.adsystem.dao.iface.AdProviderDAO;
import org.al.adsystem.model.domain.bean.Advert;
import org.hibernate.Query;
import org.hibernate.Session;
import org.hibernate.SessionFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Repository;
import org.springframework.transaction.annotation.Transactional;

import java.util.Collections;
import java.util.List;

@Repository
public class AdProviderDAOImpl implements AdProviderDAO {
    private static final int INDEX_3 = 3;

    @Autowired
    private SessionFactory sessionFactory;

    @Transactional
    @Override
    public List<Advert> getCurrentAds(final int count) {
        String hql = "from Advert a order by a.timeStamp desc";
        return getResultListByHql(hql, count);
    }

    @Transactional
    @Override
    public List<Advert> getUserAds(final int id, final int count) {
        StringBuilder builder = new StringBuilder(INDEX_3);
        builder.append("from Advert a where createdBy = ");
        builder.append(id);
        builder.append(" order by a.timeStamp desc");

        return getResultListByHql(builder.toString(), count);
    }

    @Transactional
    @Override
    public void addNewAdvert(final Advert newAd) {
        Session session = sessionFactory.getCurrentSession();
        session.save(newAd);
    }

    @Transactional
    private List<Advert> getResultListByHql(String hql, int count) {
        Session session = sessionFactory.getCurrentSession();
        Query query = session.createQuery(hql);
        query.setMaxResults(count);
        List<Advert> results = query.list();

        return Collections.unmodifiableList(results);
    }

    public void setSessionFactory(final SessionFactory sessionFactory) {
        this.sessionFactory = sessionFactory;
    }
}
