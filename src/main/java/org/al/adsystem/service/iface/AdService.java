package org.al.adsystem.service.iface;

import org.al.adsystem.model.domain.bean.Advert;
import java.util.List;

public interface AdService {

    List<Advert> getCurrentAds(int count);

    List<Advert> getUserAds(int id, int count);

    void addNewAdvert(Advert newAd);

    List<Advert> getSearchResults(String query);

}
