# Al's Ad System

The web application is an electronic bulletin board. Every anonymous user able to view published ads. And after simple registration and authorization logged users can publish their own ones.

## Motivation

The main purpose of current project - is to learn REST Web services, Spring and Hibernate frameworks by practice.

## API Reference

Maven 4, Spring framework 4.1, Hibernate 4.3, JUnit 4.11, Mockito 1.10, JSP API 2.1, Log4j 1.2.17, MySQL 5.1.18

## Installing

Compile and build of the project proceed by maven lifecycles.


## Deployment

Project running on Tomcat 8.x with default configuration settings.


## Author

* **Aliaksei Biazbubnau** - *All parts of work*
